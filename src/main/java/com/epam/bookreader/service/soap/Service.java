package com.epam.bookreader.service.soap;

import com.epam.bookreader.service.soap.dto.DTO;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface Service<T extends DTO>{
    T get(String uuid);
    List<T> getAll();
    boolean add(T t);
    boolean delete(String uuid);
    boolean update(T t);
}
