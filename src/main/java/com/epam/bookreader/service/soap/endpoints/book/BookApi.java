package com.epam.bookreader.service.soap.endpoints.book;

import com.epam.bookreader.service.soap.business.BookServiceImpl;
import com.epam.bookreader.service.soap.dto.BookDto;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService(endpointInterface = "com.epam.bookreader.service.soap.endpoints.book.BookService")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@HandlerChain(file = "../handlers.xml")
public class BookApi implements BookService {
    private BookServiceImpl  bookService = new BookServiceImpl();

    @Override
    public BookDto get(String uuid, String token) {
        return bookService.getBook(uuid);
    }

    @Override
    public List<BookDto> getAll(String token) {
        return bookService.getBooks();
    }

    @Override
    public boolean add(BookDto book, String token) {
        return bookService.addBook(book);
    }

    @Override
    public boolean delete(String uuid, String token) {
        return bookService.deleteBook(uuid);
    }

    @Override
    public boolean update(BookDto book, String token) {
        return bookService.updateBook(book);
    }
}
