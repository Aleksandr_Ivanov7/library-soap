package com.epam.bookreader.service.soap.dto;

import com.epam.bookreader.entity.User;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "user")
@XmlType(propOrder = {"uuid", "login", "password", "role"})
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDto implements DTO<User, UserDto> {
    @XmlAttribute
    private String uuid;
    private String login;
    private String password;
    @XmlElement(name = "role")
    private RoleDto role;

    public UserDto() {
    }

    public UserDto(User user) {
        uuid = user.getUUID();
        login = user.getLogin();
        password = user.getPassword();
        role = new RoleDto(user.getRole());
    }

    public String getUuid() {
        return uuid;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public RoleDto getRole() {
        return role;
    }

    @Override
    public UserDto create(User user) {
        return new UserDto(user);
    }
}
