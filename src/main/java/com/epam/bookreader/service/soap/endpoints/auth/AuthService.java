package com.epam.bookreader.service.soap.endpoints.auth;

import com.epam.bookreader.service.soap.exeption.UserNotFoundExeption;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
interface AuthService {
    String logIn(@WebParam(name = "login") String login,
                 @WebParam(name = "password") String password) throws UserNotFoundExeption;
    boolean logOut(@WebParam(name = "token") String token);
}
