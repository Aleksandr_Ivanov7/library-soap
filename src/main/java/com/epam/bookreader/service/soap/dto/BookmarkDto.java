package com.epam.bookreader.service.soap.dto;

import com.epam.bookreader.entity.Bookmark;

import javax.xml.bind.annotation.XmlAttribute;

public class BookmarkDto implements DTO<Bookmark, BookmarkDto> {
    @XmlAttribute
    private String uuid;
    private UserDto user;
    private BookDto book;
    private int page;

    public String getUuid() {
        return uuid;
    }

    public UserDto getUser() {
        return user;
    }

    public BookDto getBook() {
        return book;
    }

    public int getPage() {
        return page;
    }

    public BookmarkDto() {
    }

    public BookmarkDto(Bookmark bookmark) {
        this.uuid = bookmark.getUUID();
//        this.user = Session.currentUser(); //add session and current user
        this.book = new BookDto(bookmark.getBook());
        this.page = bookmark.getPage();
    }

    @Override
    public BookmarkDto create(Bookmark bookmark) {
        return new BookmarkDto(bookmark);
    }
}
