package com.epam.bookreader.service.soap.endpoints.user;

import com.epam.bookreader.service.soap.business.UserServiceImpl;
import com.epam.bookreader.service.soap.dto.UserDto;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService(endpointInterface = "com.epam.bookreader.service.soap.endpoints.user.UserService")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@HandlerChain(file = "../handlers.xml")
public class UserApi implements UserService{
    private UserServiceImpl userService = new UserServiceImpl();

    @Override
    public UserDto get(String uuid, String token) {
        return userService.getUser(uuid);
    }

    @Override
    public List<UserDto> getAll(String token) {
        return userService.getUsers();
    }

    @Override
    public boolean add(UserDto user, String token) {
        return userService.addUser(user);
    }

    @Override
    public boolean delete(String uuid, String token) {
        return userService.deleteUser(uuid);
    }

    @Override
    public boolean update(UserDto user, String token) {
        return userService.updateUser(user);
    }
}
