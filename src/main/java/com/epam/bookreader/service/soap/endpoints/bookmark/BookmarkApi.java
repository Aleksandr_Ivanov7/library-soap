package com.epam.bookreader.service.soap.endpoints.bookmark;

import com.epam.bookreader.service.soap.business.BookmarkServiceImpl;
import com.epam.bookreader.service.soap.dto.BookmarkDto;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService(endpointInterface = "com.epam.bookreader.service.soap.endpoints.bookmark.BookmarkService")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@HandlerChain(file = "../handlers.xml")
public class BookmarkApi implements BookmarkService {

    private BookmarkServiceImpl bookmarkService = new BookmarkServiceImpl();

    @Override
    public BookmarkDto get(String uuid, String token) {
        return bookmarkService.getBookmark(uuid);
    }

    @Override
    public List<BookmarkDto> getAll(String token) {
        return bookmarkService.getBookmarks();
    }

    @Override
    public boolean add(BookmarkDto bookmark, String token) {
        return bookmarkService.addBookmark(bookmark, token);
    }

    @Override
    public boolean delete(String uuid, String token) {
        return bookmarkService.deleteBookmark(uuid);
    }

    @Override
    public boolean update(BookmarkDto bookmark, String token) {
        return bookmarkService.updateBookmark(bookmark, token);
    }
}
