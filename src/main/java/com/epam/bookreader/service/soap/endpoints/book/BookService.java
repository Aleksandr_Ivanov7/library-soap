package com.epam.bookreader.service.soap.endpoints.book;

import com.epam.bookreader.service.soap.dto.BookDto;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface BookService {

    @WebResult(name = "book")
    BookDto get(@WebParam(name = "uuid") String uuid,
                @WebParam(name = "token", header = true) String token);

    @WebResult(name = "book")
    List<BookDto> getAll(@WebParam(name = "token", header = true) String token);

    @WebResult(name = "book")
    boolean add(@WebParam(name = "book") BookDto book,
                @WebParam(name = "token", header = true) String token);

    @WebResult(name = "book")
    boolean delete(@WebParam(name = "uuid") String uuid,
                   @WebParam(name = "token", header = true) String token);

    @WebResult(name = "book")
    boolean update(@WebParam(name = "book") BookDto book,
                   @WebParam(name = "token", header = true) String token);
}
