package com.epam.bookreader.service.soap.dto;

import com.epam.bookreader.entity.Role;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "role")
@XmlType(propOrder = {"uuid", "name"})
@XmlAccessorType(XmlAccessType.FIELD)
public class RoleDto implements DTO<Role, RoleDto>{
    @XmlAttribute
    private String uuid;
    private String name;

    public RoleDto() {
    }

    public RoleDto(Role role) {
        this.uuid = role.getUUID();
        this.name = role.getName();
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    @Override
    public RoleDto create(Role role) {
        return new RoleDto(role);
    }
}
