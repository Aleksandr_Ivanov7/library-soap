package com.epam.bookreader.service.soap.exeption;

public class NotAuthTokenExeption extends Exception {
    public NotAuthTokenExeption() {
    }

    public NotAuthTokenExeption(String message) {
        super(message);
    }

    public NotAuthTokenExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAuthTokenExeption(Throwable cause) {
        super(cause);
    }

    public NotAuthTokenExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
