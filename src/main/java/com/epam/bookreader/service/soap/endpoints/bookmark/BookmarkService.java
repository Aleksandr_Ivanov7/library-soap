package com.epam.bookreader.service.soap.endpoints.bookmark;

import com.epam.bookreader.service.soap.dto.BookmarkDto;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface BookmarkService {

    @WebResult(name = "bookmark")
    BookmarkDto get(@WebParam(name = "uuid") String uuid,
                    @WebParam(name = "token", header = true) String token);

    @WebResult(name = "bookmark")
    List<BookmarkDto> getAll(@WebParam(name = "uuid") String token);

    @WebResult(name = "bookmark")
    boolean add(@WebParam(name = "bookmark") BookmarkDto bookmark,
                @WebParam(name = "token", header = true) String token);

    @WebResult(name = "bookmark")
    boolean delete(@WebParam(name = "uuid") String uuid,
                   @WebParam(name = "token", header = true) String token);

    @WebResult(name = "bookmark")
    boolean update(@WebParam(name = "bookmark") BookmarkDto bookmark,
                   @WebParam(name = "token", header = true) String token);
}
