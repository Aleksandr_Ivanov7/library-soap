package com.epam.bookreader.service.soap.dto;

import com.epam.bookreader.service.soap.adapter.DateAdapter;
import com.epam.bookreader.entity.Author;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Date;

@XmlRootElement(name = "author")
@XmlType(propOrder = {"uuid","name", "secondName", "lastName", "dob"})
@XmlAccessorType(XmlAccessType.FIELD)
public class AuthorDto implements DTO <Author, AuthorDto>{

    @XmlAttribute
    private String uuid;
    private String name;
    private String secondName;
    private String lastName;
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date dob;
    public AuthorDto() {
    }

    public AuthorDto(Author author) {
        uuid = author.getUUID();
        name = author.getName();
        secondName = author.getSecondName();
        lastName = author.getLastName();
        dob = author.getDob();
    }

    public AuthorDto(String name, String secondName, String lastName, Date dob) {
        this.name = name;
        this.secondName = secondName;
        this.lastName = lastName;
        this.dob = dob;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getDob() {
        return dob;
    }

    @Override
    public AuthorDto create(Author author) {
        return new AuthorDto(author);
    }
}
