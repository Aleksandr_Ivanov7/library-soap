package com.epam.bookreader.service.soap.access;

import com.epam.bookreader.service.soap.session.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.NodeList;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.Set;


public class Access implements SOAPHandler<SOAPMessageContext> {
    private static final Logger log = LogManager.getLogger(Access.class);

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        Boolean outBoundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        if (!outBoundProperty) {
            try {

                SOAPHeader header = context.getMessage().getSOAPPart().getEnvelope().getHeader();
                log.info("Request");
                if (header == null) {
                    log.info("Don't have session");
                    return false;
                }
                NodeList tokenNode = header.getElementsByTagNameNS("*", "token");
                String token = tokenNode.item(0).getChildNodes().item(0).getNodeValue();
                if (!Session.isValid(token)) {
                    log.info("Not valid session");
                    return false;
                }

                //get role user
                log.info("User is "+ Session.currentUser(token).getRole().getName());


            } catch (SOAPException e) {
                e.printStackTrace();
            }
        } else {
            log.info("Response");
        }
        return true;
    }

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return false;
    }

    @Override
    public void close(MessageContext context) {

    }

//    @Resource
//    private final String VALID_PROPERTY = "RANDOM";
//    @Override
//    public boolean handleMessage(SOAPMessageContext context) {
//
//        Boolean outBoundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
//
//        // if this is an incoming message from the client
//        if (!outBoundProperty) {
//            try {
//                SOAPMessage soapMsg = context.getMessage();
//
//                SOAPEnvelope soapEnv = soapMsg.getSOAPPart().getEnvelope();
//                SOAPHeader soapHeader = soapEnv.getHeader();
//            // Grab an iterator to go through the headers
//                Iterator<?> headerIterator = soapHeader
//                        .extractHeaderElements(SOAPConstants.URI_SOAP_ACTOR_NEXT);
//
//                // if there is no additional header
//                if (headerIterator != null && headerIterator.hasNext()) {
//
//                    // Extract the property node of the header
//                    Node propertyNode = (Node) headerIterator.next();
//
//                    String property = null;
//
//                    if (propertyNode != null) {
//                        property = propertyNode.getValue();
//                        System.out.println(property);
//                    }
//
//                    if (VALID_PROPERTY.equals(property)) {
//                        // Output the message to the Console -- for debug
//                        soapMsg.writeTo(System.out);
//                    } else {
//                        // Restrict the execution of the Remote Method
//                        // Attach an error message as a response
//                        SOAPBody soapBody = soapMsg.getSOAPPart().getEnvelope().getBody();
//                        SOAPFault soapFault = soapBody.addFault();
//                        soapFault.setFaultString("Invalid Property");
//
//                        throw new SOAPFaultException(soapFault);
//                    }
//                }
//            } catch (SOAPException | IOException e) {
//                System.err.println(e);
//            }
//        }
//        return true;

    //    }
//            WebServiceContext wsctx;
}
