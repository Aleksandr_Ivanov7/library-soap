package com.epam.bookreader.service.soap.dto;

import com.epam.bookreader.entity.Entity;

public interface DTO <T extends Entity,D extends DTO> {
    D create(T t);
}
