package com.epam.bookreader.service.soap.business;

import com.epam.bookreader.dao.BookmarkDao;
import com.epam.bookreader.entity.Bookmark;
import com.epam.bookreader.service.soap.dto.BookmarkDto;
import com.epam.bookreader.service.soap.session.Session;

import java.util.List;

public class BookmarkServiceImpl implements ServiceImpl<BookmarkDto, Bookmark, BookmarkDao> {
    public List<BookmarkDto> getBookmarks(){
        return getAll(new BookmarkDao(), new BookmarkDto());
    }

    public BookmarkDto getBookmark(String uuid) {
        return get(uuid, new BookmarkDao(), new BookmarkDto());
    }

    public boolean addBookmark(BookmarkDto bookmark, String token){
        return add(transferToBookmark(bookmark ,token), new BookmarkDao());
    }

    public boolean deleteBookmark(String uuid){
        return delete(new Bookmark(uuid), new BookmarkDao());
    }

    public boolean updateBookmark(BookmarkDto bookmark, String token){
        return edit(transferToBookmark(bookmark, token), new BookmarkDao());
    }

    private Bookmark transferToBookmark(BookmarkDto bookmark, String token) {
        return new Bookmark(
                bookmark.getUuid(),
                Session.currentUser(token),
                new BookServiceImpl().transferToBook(bookmark.getBook()),
                bookmark.getPage(),
                true);
    }
}
