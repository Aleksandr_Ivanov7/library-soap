package com.epam.bookreader.service.soap.endpoints.author;

import com.epam.bookreader.service.soap.business.AuthorServiceImpl;
import com.epam.bookreader.service.soap.dto.AuthorDto;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@WebService(endpointInterface = "com.epam.bookreader.service.soap.endpoints.author.AuthorService")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@HandlerChain(file = "../handlers.xml")
public class AuthorApi implements AuthorService {

    private AuthorServiceImpl authorService = new AuthorServiceImpl();

    @Override
    public AuthorDto get(String uuid, String token) {
        return authorService.getAuthor(uuid);
    }

    @Override
    public List<AuthorDto> getAll(String token) {
        return authorService.getAuthors();
    }

    @Override
    public boolean add(String name,
                       String secondName,
                       String lastName,
                       String dob,
                       String token) throws ParseException {
        return authorService.addAuthor(new AuthorDto(name, secondName, lastName, new Date(new SimpleDateFormat("yyyy-MM-dd").parse(dob).getTime())));
    }

    @Override
    public boolean delete(String uuid, String token) {
        return authorService.deleteAuthor(uuid);
    }

    @Override
    public boolean update(AuthorDto author, String token){
        return authorService.updateAuthor(author);
    }
}

//    @Resource
//    WebServiceContext wsctx;
//        MessageContext mctx = wsctx.getMessageContext();
//
//        Map http_headers = (Map) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);
//        List userList = (List) http_headers.get("Username");
//        List passList = (List) http_headers.get("Password");
//
//        String username = "";
//        String password = "";
//
//        if (userList != null) {
//            //get username
//            username = userList.get(0).toString();
//        }
//
//        if (passList != null) {
//            //get password
//            password = passList.get(0).toString();
//        }
//        System.out.println(username + " " + password);