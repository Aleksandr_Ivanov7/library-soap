package com.epam.bookreader.service.soap.business;

import com.epam.bookreader.dao.UserDao;
import com.epam.bookreader.entity.User;
import com.epam.bookreader.service.soap.dto.UserDto;

import java.util.List;

public class UserServiceImpl implements ServiceImpl<UserDto, User, UserDao> {
    public List<UserDto> getUsers() {
        return getAll(new UserDao(), new UserDto());
    }

    public UserDto getUser(String uuid) {
        return get(uuid, new UserDao(), new UserDto());
    }

    public boolean addUser(UserDto user) {
        return add(transferToUser(user), new UserDao());
    }

    public boolean deleteUser(String uuid) {
        return delete(new User(uuid), new UserDao());
    }

    public boolean updateUser(UserDto user) {
        return edit(transferToUser(user), new UserDao());
    }

    protected User transferToUser(UserDto user) {
        return new User(
                user.getUuid(),
                user.getLogin(),
                user.getPassword(),
                new RoleServiceImpl().transferToRole(user.getRole()),
                true
        );
    }
}
