package com.epam.bookreader.service.soap.endpoints.role;

import com.epam.bookreader.service.soap.business.RoleServiceImpl;
import com.epam.bookreader.dao.RoleDao;
import com.epam.bookreader.service.soap.dto.RoleDto;
import com.epam.bookreader.entity.Role;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService(endpointInterface = "com.epam.bookreader.service.soap.endpoints.role.RoleService")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@HandlerChain(file = "../handlers.xml")
public class RoleApi implements RoleService{

    private RoleServiceImpl roleService = new RoleServiceImpl();

    @Override
    public RoleDto get(String uuid, String token) {
        return roleService.getRole(uuid);
    }

    @Override
    public List<RoleDto> getAll(String token) {
        return roleService.getRoles();
    }

    @Override
    public boolean add(RoleDto roleDto, String token) {
        return roleService.addRole(roleDto);
    }

    @Override
    public boolean delete(String uuid, String token) {
        return roleService.delete(new Role(uuid), new RoleDao());
    }

    @Override
    public boolean update(RoleDto roleDto, String token) {
        return roleService.updateRole(roleDto);
    }
}
