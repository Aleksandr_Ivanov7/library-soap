package com.epam.bookreader.service.soap.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAdapter extends XmlAdapter<String, Date> {

    //from xml
    @Override
    public Date unmarshal(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = format.parse(date);
        return new java.sql.Date(parsed.getTime());
    }

    //to xml
    @Override
    public String marshal(Date date) {
        return date.toString();
    }
}
