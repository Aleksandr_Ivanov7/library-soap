package com.epam.bookreader.service.soap.business;

import com.epam.bookreader.dao.AuthorDao;
import com.epam.bookreader.service.soap.dto.AuthorDto;
import com.epam.bookreader.entity.Author;

import java.util.List;

public class AuthorServiceImpl implements ServiceImpl<AuthorDto, Author, AuthorDao>{

    public List<AuthorDto> getAuthors() {
        return getAll(new AuthorDao(), new AuthorDto());
    }

    public AuthorDto getAuthor(String uuid) {
        return get(uuid, new AuthorDao(), new AuthorDto());
    }

    public boolean addAuthor(AuthorDto authorDto) {
        return add(transferToAuthor(authorDto), new AuthorDao());
    }

    public boolean deleteAuthor(String uuid) {
        return delete(new Author(uuid), new AuthorDao());
    }

    public boolean updateAuthor(AuthorDto author) {
        return edit(transferToAuthor(author), new AuthorDao());
    }

    private Author transferToAuthor(AuthorDto author) {
        return new Author(
                author.getUuid(),
                author.getName(),
                author.getSecondName(),
                author.getLastName(),
                author.getDob(),
                true
        );
    }
}
