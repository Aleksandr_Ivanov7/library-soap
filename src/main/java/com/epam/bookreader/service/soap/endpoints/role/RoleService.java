package com.epam.bookreader.service.soap.endpoints.role;

import com.epam.bookreader.service.soap.dto.RoleDto;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface RoleService {
    @WebResult(name = "role")
    RoleDto get(@WebParam(name = "uuid") String uuid,
                @WebParam(name = "token", header = true) String token);

    @WebResult(name = "role")
    List<RoleDto> getAll(@WebParam(name = "token", header = true) String token);

    @WebResult(name = "role")
    boolean add(@WebParam(name = "role")RoleDto role,
                @WebParam(name = "token", header = true) String token);

    @WebResult(name = "role")
    boolean delete(@WebParam(name = "uuid")String uuid,
                   @WebParam(name = "token", header = true) String token);

    @WebResult(name = "role")
    boolean update(@WebParam(name = "role")RoleDto role,
                   @WebParam(name = "token", header = true) String token);
}
