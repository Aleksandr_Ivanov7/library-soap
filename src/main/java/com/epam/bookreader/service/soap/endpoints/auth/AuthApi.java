package com.epam.bookreader.service.soap.endpoints.auth;

import com.epam.bookreader.dao.UserDao;
import com.epam.bookreader.entity.User;
import com.epam.bookreader.service.soap.exeption.UserNotFoundExeption;
import com.epam.bookreader.service.soap.session.Session;
import org.apache.commons.codec.digest.DigestUtils;

import javax.jws.WebService;
import java.sql.SQLException;
import java.util.UUID;

@WebService(endpointInterface = "com.epam.bookreader.service.soap.endpoints.auth.AuthService")
public class AuthApi implements AuthService{

    @Override
    public String logIn(String login, String password){
        try {
            User user = new UserDao().get(new User(login, DigestUtils.md5Hex(password)));
            if(new UserDao().get(user) == null) throw new UserNotFoundExeption();
            String auth_token = UUID.randomUUID().toString();
            Session.add(user, auth_token);
            return auth_token;
        } catch (SQLException | UserNotFoundExeption e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean logOut(String auth_token) {
        return Session.delete(Session.currentUser(auth_token));
    }
}
