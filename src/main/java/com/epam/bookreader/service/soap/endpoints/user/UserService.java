package com.epam.bookreader.service.soap.endpoints.user;

import com.epam.bookreader.service.soap.dto.UserDto;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface UserService {
    @WebResult(name = "user")
    UserDto get(@WebParam(name = "uuid") String uuid,
                @WebParam(name = "token", header = true) String token);

    @WebResult(name = "user")
    List<UserDto> getAll(@WebParam(name = "token", header = true) String token);

    @WebResult(name = "user")
    boolean add(@WebParam(name = "user", header = true) UserDto user,
                @WebParam(name = "token", header = true) String token);

    @WebResult(name = "user")
    boolean delete(@WebParam(name = "uuid") String uuid,
                   @WebParam(name = "token", header = true) String token);

    @WebResult(name = "user")
    boolean update(@WebParam(name = "user", header = true) UserDto user,
                   @WebParam(name = "token", header = true) String token);
}
