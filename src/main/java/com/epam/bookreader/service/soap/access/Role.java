package com.epam.bookreader.service.soap.access;

public enum Role {
    ADMIN,
    MODERATOR,
    USER
}
