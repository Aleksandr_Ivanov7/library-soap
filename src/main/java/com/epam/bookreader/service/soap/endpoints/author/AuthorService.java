package com.epam.bookreader.service.soap.endpoints.author;

import com.epam.bookreader.service.soap.access.AccessRole;
import com.epam.bookreader.service.soap.access.Role;
import com.epam.bookreader.service.soap.dto.AuthorDto;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@WebService
@AccessRole({Role.ADMIN})
public interface AuthorService {

    @WebResult(name = "author")
    AuthorDto get(@WebParam(name = "uuid") String uuid,
                  @WebParam(name = "token", header = true) String token);

    @WebResult(name = "author")
    List<AuthorDto> getAll(@WebParam(name = "token", header = true) String token);

    @WebResult(name = "author")
    boolean add(@WebParam(name = "name") String name,
                @WebParam(name = "secondName") String secondName,
                @WebParam(name = "lastName") String lastName,
                @WebParam(name = "dob") String dob,
                @WebParam(name = "token", header = true) String token) throws ParseException;

    @WebResult(name = "author")
    boolean delete(@WebParam(name = "uuid") String uuid,
                   @WebParam(name = "token", header = true) String token);

    @WebResult(name = "author")
    boolean update(@WebParam(name = "author") AuthorDto author,
                   @WebParam(name = "token", header = true) String token) throws ParseException;
}
