package com.epam.bookreader.service.soap.session;

import com.epam.bookreader.entity.User;

import java.util.HashMap;
import java.util.Map;

public class Session {
    private static Map<User, String> tokens;

    public static void add(User user, String token) {
        if (tokens == null) init();
        tokens.put(user, token);
    }

    public static boolean delete(User user) {
        if (tokens == null) return false;
        tokens.remove(user);
        return true;
    }

    private static void init() {
        tokens = new HashMap<>();
    }

    public static boolean isValid(String token) {
        if (tokens == null) return false;
        return tokens.containsValue(token);
    }

    public static User currentUser(String token) {
        for (Map.Entry entry : tokens.entrySet())
            if (entry.getValue().equals(token)) return (User) entry.getKey();
        return null;
    }
}
