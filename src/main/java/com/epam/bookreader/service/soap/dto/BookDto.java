package com.epam.bookreader.service.soap.dto;

import com.epam.bookreader.entity.Author;
import com.epam.bookreader.entity.Book;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "book")
@XmlType(propOrder = {"uuid", "isbn", "bookName", "publisher", "releaseYear", "pageCount", "author"})
@XmlAccessorType(XmlAccessType.FIELD)
public class BookDto implements DTO<Book, BookDto> {

    @XmlAttribute
    private String uuid;
    private String isbn;
    private String bookName;
    private String publisher;
    private int releaseYear;
    private int pageCount;
    @XmlElement(name = "author")
    private AuthorDto author;

    public String getUuid() {
        return uuid;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getBookName() {
        return bookName;
    }

    public String getPublisher() {
        return publisher;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public int getPageCount() {
        return pageCount;
    }

    public AuthorDto getAuthor() {
        return author;
    }

    public BookDto() {
    }

    public BookDto(String uuid, String isbn, String bookName, String publisher, int releaseYear, int pageCount, AuthorDto author) {
        this.uuid = uuid;
        this.isbn = isbn;
        this.bookName = bookName;
        this.publisher = publisher;
        this.releaseYear = releaseYear;
        this.pageCount = pageCount;
        this.author = author;
    }

    public BookDto(Book book) {
        this.uuid = book.getUUID();
        this.isbn = book.getIsbn();
        this.bookName = book.getBookName();
        this.publisher = book.getPublisher();
        this.releaseYear = book.getReleaseYear();
        this.pageCount = book.getPageCount();
        this.author = new AuthorDto(book.getAuthor() == null ? new Author() : book.getAuthor());
    }

    @Override
    public BookDto create(Book book) {
        return new BookDto(book);
    }
}
