package com.epam.bookreader.service.soap.business;

import com.epam.bookreader.dao.BookDao;
import com.epam.bookreader.entity.Author;
import com.epam.bookreader.entity.Book;
import com.epam.bookreader.service.soap.dto.BookDto;

import java.util.List;

public class BookServiceImpl implements ServiceImpl<BookDto, Book, BookDao> {
    public List<BookDto> getBooks() {
        return getAll(new BookDao(), new BookDto());
    }

    public BookDto getBook(String uuid) {
        return get(uuid, new BookDao(), new BookDto());
    }

    public boolean addBook(BookDto book) {
        return add(transferToBook(book), new BookDao());
    }

    public boolean deleteBook(String uuid) {
        return delete(new Book(uuid), new BookDao());
    }

    public boolean updateBook(BookDto book) {
        return edit(transferToBook(book), new BookDao());
    }

    protected Book transferToBook(BookDto book) {
        return new Book(
                book.getUuid(),
                book.getIsbn(),
                book.getBookName(),
                book.getPublisher(),
                book.getReleaseYear(),
                book.getPageCount(),
                true,
                new Author(
                        book.getUuid(),
                        book.getAuthor().getName(),
                        book.getAuthor().getSecondName(),
                        book.getAuthor().getLastName(),
                        book.getAuthor().getDob(),
                        true)
        );
    }
}
