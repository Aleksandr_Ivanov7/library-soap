package com.epam.bookreader.service.soap.business;

import com.epam.bookreader.dao.Dao;
import com.epam.bookreader.service.soap.dto.DTO;
import com.epam.bookreader.entity.Entity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

interface ServiceImpl <T extends DTO<E, T>, E extends Entity, D extends Dao<E>>
{
    default List<T> getAll(D dao, T dto){
        List<T> entitiesDto = new ArrayList<>();
        try {
            for (E entity : dao.getAll()) if(entity.isActive()) entitiesDto.add(dto.create(entity));
            return entitiesDto;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    default T get(String uuid, D dao, T dto){
        try {
            return dto.create(dao.get(uuid));
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    default boolean add(E e, D dao){
        try {
            return dao.save(e);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    default boolean delete(E e, D dao){
        try {
            return dao.delete(e);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    default boolean edit(E e, D dao){
        try {
            return dao.update(e);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
