package com.epam.bookreader.service.soap.business;

import com.epam.bookreader.dao.RoleDao;
import com.epam.bookreader.entity.Role;
import com.epam.bookreader.service.soap.dto.RoleDto;

import java.util.List;

public class RoleServiceImpl implements ServiceImpl<RoleDto, Role, RoleDao> {
    public List<RoleDto> getRoles() {
        return getAll(new RoleDao(), new RoleDto());
    }

    public RoleDto getRole(String uuid) {
        return get(uuid, new RoleDao(), new RoleDto());
    }

    public boolean addRole(RoleDto role) {
        return add(transferToRole(role), new RoleDao());
    }

    public boolean deleteRole(String name) {
        return delete(new Role(name), new RoleDao());
    }

    public boolean updateRole(RoleDto role) {
        return edit(transferToRole(role), new RoleDao());
    }

    Role transferToRole(RoleDto role) {
        return new Role(
                role.getUuid(),
                role.getName(),
                true);
    }

}
